name := "tfidf"

version := "1.0"

scalaVersion := "2.10.3"

scalacOptions ++= Seq("-deprecation")

libraryDependencies ++= Seq(
  "org.apache.hadoop" % "hadoop-client" % "2.5.0-cdh5.3.0",
//  "org.apache.hadoop" % "hadoop-core" % "2.5.0-mr1-cdh5.3.0",
  "org.apache.hadoop" % "hadoop-common" % "2.5.0-cdh5.3.0",
  "org.apache.hadoop" % "hadoop-hdfs" % "2.5.0-cdh5.3.0",
//  "org.apache.hadoop" % "hadoop-annotations" % "2.5.0-cdh5.3.0",
//  "org.apache.hadoop" % "hadoop-aws" % "2.5.0-cdh5.3.0",
  "org.apache.hadoop" % "hadoop-mapreduce-client-app" % "2.5.0-cdh5.3.0",
  "org.apache.hadoop" % "hadoop-mapreduce-client-common" % "2.5.0-cdh5.3.0",
  "org.apache.hadoop" % "hadoop-mapreduce-client-core" % "2.5.0-cdh5.3.0"
//  "org.apache.hadoop" % "hadoop-mapreduce-client-shuffle" % "2.5.0-cdh5.3.0"
)

resolvers ++= Seq("cloudera" at "https://repository.cloudera.com/content/repositories/releases",
  "cloudera-repos" at "https://repository.cloudera.com/content/groups/cloudera-repos"
)

//import com.github.retronym.SbtOneJar._

//oneJarSettings
