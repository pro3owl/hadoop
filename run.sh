#!/bin/sh
set -e
#export LIBJARS=$(dirname $0)/scala-library.jar
#export HADOOP_CLASSPATH=/home/cloudera/scala-tfidf/scala-library.jar
sbt package
rm -rf tmp wordcount.jar || true
mkdir tmp
(cd tmp && jar -xvf ../target/scala-2.10/tfidf_2.10-1.0.jar 1>/dev/null)
(cd tmp && jar -xvf ../scala-library.jar 1>/dev/null)
jar -cvf main.jar -C tmp . 1>/dev/null
#hadoop fs -rm -r /user/cloudera/tfidf/output || true
hadoop jar main.jar com.proowl.tfidf.Main /user/cloudera/tfidf/data.json /user/cloudera/tfidf/output
#hadoop fs -cat /user/cloudera/tfidf/output/*
