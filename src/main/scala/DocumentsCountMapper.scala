package com.proowl.tfidf

import java.io.{OutputStreamWriter, BufferedWriter}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapreduce.{Reducer, Mapper}

import scala.util.parsing.json.JSON

/**
 * Created by proowl on 07/04/15.
 */
class DocumentsCountMapper extends Mapper[Object,Text,Text,IntWritable] {
  val one = new IntWritable(1)
  val document_flag = new Text("document")

  override
  def map(key: Object, value: Text, context: Mapper[Object,Text,Text,IntWritable]#Context) {
    JSON.parseFull(value.toString()) match {
      case Some(json_map : Map[String, Any]) => context.write(document_flag, one)
      case None => println("Parsing failed")
      case other => println("Unknown data structure: " + other)
    }
  }
}