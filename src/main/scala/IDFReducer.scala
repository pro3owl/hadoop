package com.proowl.tfidf

import java.io.{InputStreamReader, BufferedReader}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.{DoubleWritable, IntWritable, Text}
import org.apache.hadoop.mapreduce.{Reducer}
import scala.collection.JavaConversions._

/**
 * Created by proowl on 08/04/15.
 */
class IDFReducer extends Reducer[Text,IntWritable,Text,DoubleWritable] {
  val docs_count_path = new Path("hdfs://quickstart.cloudera:8020/user/cloudera/tfidf/output/docs_count/part-r-00000")
  val fs = FileSystem.get(new Configuration())
  val docs_count = new BufferedReader(new InputStreamReader(fs.open(docs_count_path)))
    .readLine().split("\\t")(1).toInt

  override
  def reduce(key: Text, values: java.lang.Iterable[IntWritable],
             context: Reducer[Text,IntWritable,Text,DoubleWritable]#Context) {
    val sum = values.foldLeft(0) { (t,i) => t + i.get }
    context.write(key, new DoubleWritable(math.log(1.0 * docs_count / sum)))
  }
}


