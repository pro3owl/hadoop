package com.proowl.tfidf

import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapreduce.Reducer
import scala.collection.JavaConversions._

/**
 * Created by proowl on 07/04/15.
 */
class SumReducer extends Reducer[Text,IntWritable,Text,IntWritable] {
  override
  def reduce(key: Text, values: java.lang.Iterable[IntWritable],
             context: Reducer[Text,IntWritable,Text,IntWritable]#Context) {
    val sum = values.foldLeft(0) { (t,i) => t + i.get }
    context.write(key, new IntWritable(sum))
  }
}

