package com.proowl.tfidf

import java.io.{IOException, InputStreamReader, BufferedReader}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.{MapWritable, DoubleWritable, IntWritable, Text}
import org.apache.hadoop.mapred.JobConf
import org.apache.hadoop.mapreduce.{Mapper, Reducer}
import scala.collection.JavaConversions._
import scala.collection.mutable.HashMap
import scala.io.Source

import scala.util.parsing.json.JSON

/**
 * Created by proowl on 08/04/15.
 */
class TFIDFMapper extends Mapper[Object,Text,Text,Text] {
  val idfCache = new HashMap[String, Double]

  override
  def setup(context: Mapper[Object, Text, Text, Text]#Context) {
    // Get the cached tokens' idf-s
    val uri = context.getCacheFiles()(0)
    val fs = FileSystem.get(new Configuration())
    val statuses = fs.listStatus(new Path(uri))
    for (status <- statuses) {
      val reader = Source.fromInputStream(fs.open(status.getPath()))
      for (line: String <- reader.getLines()) {
        val splitted = line.split("\\t")
        idfCache.put(splitted(0), splitted(1).toDouble)
      }
    }
  }

  override
  def map(key: Object, value: Text, context: Mapper[Object,Text,Text,Text]#Context) {
    JSON.parseFull(value.toString()) match {
      case Some(json_map : Map[String, Any]) => map_json(json_map, context)
      case None => println("Parsing failed")
      case other => println("Unknown data structure: " + other)
    }
  }

  def map_json(json_map: Map[String, Any], context: Mapper[Object,Text,Text,Text]#Context) {
    val tokensList = json_map.get("tokens").get.asInstanceOf[List[String]]
    val tokensToFreq = new HashMap[String, Double]
    tokensList.foreach(token => tokensToFreq(token) = tokensToFreq.getOrElse(token, 0.0) + 1)
    tokensToFreq.foreach({ case (token, value) => tokensToFreq(token) = value / tokensList.length * idfCache(token) })

    val mapped = new HashMap[String, Any]
    mapped.put("topics", json_map.get("topics").get.asInstanceOf[List[String]])
    mapped.put("tfidf", tokensToFreq)

    context.write(new Text(json_map.get("url").get.asInstanceOf[String]), new Text(mapped.mkString))
  }
}
