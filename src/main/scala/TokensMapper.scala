package com.proowl.tfidf

import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapreduce.{Reducer, Mapper}

import scala.util.parsing.json.JSON

/**
 * Created by proowl on 07/04/15.
 */
class TokensMapper(fieldName: String) extends Mapper[Object,Text,Text,IntWritable] {
  val one = new IntWritable(1)
  val field = fieldName

  override
  def map(key: Object, value: Text, context: Mapper[Object,Text,Text,IntWritable]#Context) {
    JSON.parseFull(value.toString()) match {
      case Some(json_map : Map[String, Any]) => map_tokens(json_map, context)
      case None => println("Parsing failed")
      case other => println("Unknown data structure: " + other)
    }
  }

  def map_tokens(json_map: Map[String, Any], context: Mapper[Object,Text,Text,IntWritable]#Context) {
    val tokens_opt = json_map get field
    tokens_opt.get.asInstanceOf[List[String]]
      .map { x => new Text(x) }
      .map { x => context.write(x, one) }
  }
}