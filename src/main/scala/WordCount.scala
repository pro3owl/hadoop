package com.proowl.tfidf

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs._
import org.apache.hadoop.io.{MapWritable, DoubleWritable, IntWritable, Text}
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.util.GenericOptionsParser
import java.io.{InputStreamReader, BufferedReader, PrintStream}
import org.apache.log4j._

object Main {
  def documentsCount(conf: Configuration, input: String, output: String): Job = {
    val job = new Job(conf, "documents count")
    job.setJarByClass(classOf[DocumentsCountMapper])
    job.setMapperClass(classOf[DocumentsCountMapper])
    job.setCombinerClass(classOf[SumReducer])
    job.setReducerClass(classOf[SumReducer])
    job.setOutputKeyClass(classOf[Text])
    job.setOutputValueClass(classOf[IntWritable])
    FileInputFormat.addInputPath(job, new Path(input))
    FileOutputFormat.setOutputPath(job, new Path(output))

    job
  }

  def topicsSums(conf: Configuration, input: String, output: String): Job = {
    class mapper extends TokensMapper("topics") {}

    val job = new Job(conf, "topics sums (task 1)")
    job.setJarByClass(classOf[mapper])
    job.setMapperClass(classOf[mapper])
    job.setCombinerClass(classOf[SumReducer])
    job.setReducerClass(classOf[SumReducer])
    job.setOutputKeyClass(classOf[Text])
    job.setOutputValueClass(classOf[IntWritable])
    FileInputFormat.addInputPath(job, new Path(input))
    FileOutputFormat.setOutputPath(job, new Path(output))

    job
  }

  def makeIDF(conf: Configuration, input: String, output: String): Job = {
    class mapper extends TokensMapper("tokens") {}

    val job = new Job(conf, "tokens sums (part, task 2)")
    job.setJarByClass(classOf[mapper])
    job.setMapperClass(classOf[mapper])
    job.setReducerClass(classOf[IDFReducer])
    job.setMapOutputKeyClass(classOf[Text])
    job.setMapOutputValueClass(classOf[IntWritable])
    job.setOutputKeyClass(classOf[Text])
    job.setOutputValueClass(classOf[DoubleWritable])
    FileInputFormat.addInputPath(job, new Path(input))
    FileOutputFormat.setOutputPath(job, new Path(output))

    job
  }

  def makeTFIDF(conf: Configuration, input: String, output: String): Job = {
    val job = new Job(conf, "tfidf (task 3)")

    val idf_path = new Path("hdfs://quickstart.cloudera:8020/user/cloudera/tfidf/output/tokens_idf")
    job.addCacheFile(idf_path.toUri())

    job.setJarByClass(classOf[TFIDFMapper])
    job.setMapperClass(classOf[TFIDFMapper])
    job.setOutputKeyClass(classOf[Text])
    job.setOutputValueClass(classOf[Text])
    FileInputFormat.addInputPath(job, new Path(input))
    FileOutputFormat.setOutputPath(job, new Path(output))

    job
  }

  def main(args:Array[String]):Int = {
    System.setOut( new PrintStream( new LoggingOutputStream( Logger.getRootLogger(), Level.INFO ), true))

    val conf = new Configuration()
    val otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs
    if (otherArgs.length < 2 || otherArgs.length > 3) {
      println("Usage: <in> <out> [step]")
      return 2
    }

    val inputPath = args(0)
    val outputPath = args(1)
    val step = if (otherArgs.length < 3) "prepare" else args(2)

    val result = step match {
      case "prepare" => {
        val docsCountJob = documentsCount(conf, inputPath, outputPath + "/docs_count")
        val topicsSumsJob = topicsSums(conf, inputPath, outputPath + "/topics")

        docsCountJob.waitForCompletion(true) &&
          topicsSumsJob.waitForCompletion(true)
      }
      case "idf" => {
        makeIDF(conf, inputPath, outputPath + "/tokens_idf")
          .waitForCompletion(true)
      }
      case "tfidf" => {
        makeTFIDF(conf, inputPath, outputPath + "/tf_idf")
          .waitForCompletion(true)
      }
    }

    if (result) 0 else 1
  }

}
